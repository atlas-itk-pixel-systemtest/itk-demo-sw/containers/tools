# DeMi tools

This repository contains a maintained `docker compose` file (DeMi "stack") of general tools / backing services for DeMi (detector microservices).

Standard scripts to bootstrap Docker and write the required environment variables to `.env` for site-wide shared tools needed by DeMi microservices are provided.

Usage:

```shell
./bootstrap
./config
docker compose up -d
```

- `bootstrap` this script sets up all dependencies needed to bring up the containers.
- `config` this script writes the `.env` configuration.

## Portainer and Traefik

[Portainer](https://www.portainer.io/)
and [Traefik](https://doc.traefik.io/traefik/) are a special case because they are used to manage all other containers. They are therefore started separately, and in a system-wide mannner.

Their use remains optional. You can just use docker compose and/or pydemi.

To use Portainer and Traefik:

```shell
./bootstrap
cd portainer
./config
docker compose up -d
```

This should be done before starting the tools stack.
The tools stack can then be imported into portainer by 
- uploading `stack.env` to Portainer.
- pointing directly at this Git-repo:
`https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/tools.git`

---
Contact: G. Brandt <gbrandt@cern.ch>

